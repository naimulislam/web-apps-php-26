<?php
// php.net

/*$var = '';

if (isset($var)){
    echo "This var is set so i will print.";
}

$a = "test";
$b = "anothertest";

var_dump(isset($a));
var_dump(isset($a, $b));

unset($a);

var_dump(isset($a));
var_dump(isset($a, $b));

$foo = NULL;
var_dump(isset($foo));

$a = array('test' => 1, 'hell0' => NULL, 'pie' => array('a' => 'apple'));

var_dump(isset($a['test']));
var_dump(isset($a['foo']));
var_dump(isset($a['hell0']));

var_dump(array_key_exists('hello','$a'));

var_dump(isset($a['pie']['a']));
var_dump(isset($a['pie']['b']));
var_dump(isset($a['cake']['c']['b']));*/

$expected_array_got_string = 'somestring';
var_dump(isset($expected_array_got_string['some_key']));
var_dump(isset($expected_array_got_string[0]));
var_dump(isset($expected_array_got_string['0']));
var_dump(isset($expected_array_got_string[0.5]));
var_dump(isset($expected_array_got_string['0.5']));
var_dump(isset($expected_array_got_string['0 mostel']));

?>