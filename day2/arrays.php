<?php
// php.net

// #1 A simple array

/*array = array(
    "foo" => "bar",
    "bar" => "foo",
);
// as of PHP 5.4
$array = [
    "foo"=>"bar",
    "bar"=>"foo",
];*/

// #2 Type Casting and Overwriting example

/*$array=array(
    1=>"a",
    "1"=>"b",
    1.5=>"c",
    true=>"d",
);
var_dump($array);*/

// #3 Mixed integer and string keys

/*$array=array(
    "foo"=>"bar",
    "bar"=>"foo",
    100=>-100,
    -100=>100,
);
var_dump($array);*/

// #4 Indexed arrays without key

//$array=array("foo","bar","hello","world");
//var_dump($array);

// #5 Keys not on all elements

/*$array=array(
    "a",
    "b",
    6=>"c",
    "d",
);
var_dump($array);*/

// #6 Accessing array elements

/*$array = array(
    "foo" => "bar",
    42 => 24,
    "multi" => array(
        "dimensional" => array(
            "array" => "foo"
        )
    )
);
var_dump($array["foo"]);
var_dump($array[42]);
var_dump($array["multi"]["dimensional"]["array"]);*/

// #7 Array dereferencing

/*function getArray(){
    return array(1, 2, 3);
}

// on php 5.4
$secondElement = getArray()[1];

// previously
$tmp = getArray();
$secondElement = $tmp[1];

// or
list(, $secondElement) = getArray();*/

/*$arr = array(5 => 1, 12 => 2);
$arr[] = 56;
$arr["x"] = 42;
unset($arr[5]);
unset($arr);*/

?>